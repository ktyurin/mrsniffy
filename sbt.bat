set SCRIPT_DIR=%~dp0
%JAVA_HOME%\bin\java -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256m -Xms256M -Xmx256M -Xss2M -jar "%SCRIPT_DIR%\sbt-launch.jar" %*
