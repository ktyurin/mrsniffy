name := "mrsniffy"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.10.1"

organization := "com.mrsniffy"

seq(webSettings :_*)

libraryDependencies ++= Seq(
        "ch.qos.logback" % "logback-classic" % "1.0.11",
        "com.typesafe" %% "scalalogging-slf4j" % "1.0.1",
		"net.liftweb" %% "lift-webkit" % "2.5-RC4" % "compile->default",
		"org.mortbay.jetty" % "jetty" % "6.1.26" % "container"
    )