package com.mrsniffy.snippet

import net.liftweb.util.Helpers._
import net.liftweb.http.{S, StreamingResponse, ResponseShortcutException, SHtml}
import com.typesafe.scalalogging.slf4j.Logging
import net.liftweb.sitemap.{Loc, Menu}
import java.net.{MalformedURLException, URL}

class Download extends Logging {
  def hello = {
    var link = ""

    def onSubmit() {
      var dataIsValid = true

      if (link.isEmpty) {
        S.error(Download.linkMsg, "Please enter the link")
        dataIsValid = false
      }

      if (dataIsValid) {
        try {
          val con = new URL(link).openConnection()
          val in = con.getInputStream

          val headers =
            ("Content-type" -> "application/octet-stream") ::
              ("Content-disposition" -> "attachment; filename=file") :: Nil

          throw ResponseShortcutException.shortcutResponse(StreamingResponse(in, () => {
            in.close
          }, con.getContentLengthLong, headers, Nil, 200))
        } catch {
          case e: MalformedURLException => S.error(Download.linkMsg, "The URL you entered is not valid")
        }
      }
    }

    Download.linkId #> SHtml.text("", link = _) &
      Download.submitId #> SHtml.submit("Download", onSubmit _)
  }
}

object Download {
  lazy val menu = Menu(Loc("Download", List("index"), "Download"))
  private val linkId = "#l-index-link"
  private val submitId = "#l-index-submit"
  private val linkMsg = "m-index-link"
}