package com.mrsniffy.commet

import net.liftweb.http.{S, CometActor}
import net.liftweb.util.PassThru
import net.liftweb.util.Helpers._
import net.liftweb.common.Full

trait SimpleActor extends CometActor {
  private val session = S.session.get

  override def render = PassThru

  def put = "#l-simpleActor" #> <span class={s"lift:comet?type=${getClass.getSimpleName};name=${uniqueId}"}></span>

  def sendMessage(msg: Any) {
    session.sendCometActorMessage(getClass.getSimpleName, Full(uniqueId), msg)
  }
}