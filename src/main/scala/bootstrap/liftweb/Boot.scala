package bootstrap.liftweb

import net.liftweb.http.{GetRequest, Html5Properties, Req, LiftRules}
import net.liftweb.common.Full
import com.typesafe.scalalogging.slf4j.Logging
import net.liftweb.sitemap.SiteMap
import com.mrsniffy.snippet.Download

class Boot extends Logging {
  def boot {
    LiftRules.ajaxStart = Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)
    LiftRules.ajaxEnd = Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))
    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent))
    LiftRules.addToPackages("com.mrsniffy")

    LiftRules.setSiteMap(SiteMap(
      Download.menu
    ))



    LiftRules.liftRequest.append {
      case Req("classpath" :: _, _, _) => true
      case Req("ajax_request" :: _, _, _) => true
      case Req("favicon" :: Nil, "ico", GetRequest) => false
      case Req(_, "css", GetRequest) => false
      case Req(_, "js", GetRequest) => false
    }
  }
}