#!/bin/bash

$JAVA_HOME/bin/java -Xms256M -Xmx256M -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=256M -jar `dirname $0`/sbt-launch.jar "$@"